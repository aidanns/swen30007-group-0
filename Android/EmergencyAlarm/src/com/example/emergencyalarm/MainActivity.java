package com.example.emergencyalarm;

import com.example.emergencyalarm.R;
import de.passsy.holocircularprogressbar.HoloCircularProgressBar;
import android.os.Bundle;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	
	/**
	 * The animate button.
	 */
	private Button animateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Create the progress bar.
		final HoloCircularProgressBar progress = (HoloCircularProgressBar) findViewById(R.id.circularProgressBar);
        
		// Create the button that starts animation.
        animateButton = (Button) findViewById(R.id.animateButton);
        animateButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				animate(progress, new AnimatorListener() {

					@Override
					public void onAnimationCancel(final Animator animation) {
					}

					@Override
					public void onAnimationEnd(final Animator animation) {
						animate(progress, this);
					}

					@Override
					public void onAnimationRepeat(final Animator animation) {
					}

					@Override
					public void onAnimationStart(final Animator animation) {
					}
				});
			}
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
	/**
	 * Animate.
	 * 
	 * @param progressBar
	 *            the progress bar
	 * @param listener
	 *            the listener
	 */
	private void animate(final HoloCircularProgressBar progressBar, final AnimatorListener listener) {
		final float progress = (float) (Math.random() * 2);
		final ObjectAnimator progressBarAnimator = ObjectAnimator.ofFloat(progressBar, "progress", progress);
		progressBarAnimator.setDuration(3000);

		progressBarAnimator.addListener(new AnimatorListener() {

			@Override
			public void onAnimationCancel(final Animator animation) {
			}

			@Override
			public void onAnimationEnd(final Animator animation) {
				progressBar.setProgress(progress);
			}

			@Override
			public void onAnimationRepeat(final Animator animation) {
			}

			@Override
			public void onAnimationStart(final Animator animation) {
			}
		});
		progressBarAnimator.addListener(listener);
		progressBarAnimator.reverse();
		progressBarAnimator.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(final ValueAnimator animation) {
				progressBar.setProgress((Float) animation.getAnimatedValue());
			}
		});
		progressBar.setMarkerProgress(progress);
		progressBarAnimator.start();
	}
    
}
