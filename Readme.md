# SWEN30007 Group 0 Readme

## Overview.

This is an Emergency Alarm created for the final semester project in the Bachelor of Science (Software Systems) at The University of Melbourne. The alarm is designed to allow persons in need of assistance to notify their carers that they are in need of help.

## Building.

### iOS

Open `iOD/EmergencyAlarm.xcodeworkspace` in XCode and run the `EmergencyAlarm` project in the simulator.

### Android

Import the projects in the `Android/` directory in to a new ADT workspace and run the `EmergencyAlarm` project in the simulator.

