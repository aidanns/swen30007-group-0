var testName = "Press Button";
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();

var button = window.buttons()["Button"];
button.tap();

var firstNameLabel = window.staticTexts()["First Name"];
var lastNameLabel = window.staticTexts()["Last Name"];

if (firstNameLabel && lastNameLabel) {
	UIALogger.logPass(testName);
} else {
	UIALogger.logFail(testName);
}

