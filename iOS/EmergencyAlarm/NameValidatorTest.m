//
//  NameValidatorTest.m
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#define HC_SHORTHAND
#import <OCHamcrest/OCHamcrest.h>

#define MOCKITO_SHORTHAND
#import <OCMockito/OCMockito.h>

#import "NameValidatorTest.h"

#import "NameValidator.h"

NSString * const firstName = @"Luke";
NSString * const lastName = @"Skywalker";
NSString * const invalidFirstName = @"";
NSString * const invalidLastName = @"";

@interface NameValidatorTest ()

@property (strong, nonatomic) NameValidator *validator;

@property (strong, nonatomic) FirstNameValidator *mockFirstNameValidator;
@property (strong, nonatomic) LastNameValidator *mockLastNameValidator;

@end

@implementation NameValidatorTest

- (void)setUp {
    [super setUp];

    self.mockFirstNameValidator = mock([FirstNameValidator class]);
    self.mockLastNameValidator = mock([LastNameValidator class]);
    
    [given([self.mockFirstNameValidator validate:firstName]) willReturnBool:YES];
    [given([self.mockLastNameValidator validate:lastName]) willReturnBool: YES];
    [given([self.mockFirstNameValidator validate:invalidFirstName]) willReturnBool:NO];
    [given([self.mockLastNameValidator validate:invalidLastName]) willReturnBool:NO];
    
    self.validator = [[NameValidator alloc] initWithFirstNameValidator:self.mockFirstNameValidator andLastNameValidator:self.mockLastNameValidator];
}

- (void)tearDown {
    [super tearDown];
    
    self.mockFirstNameValidator = nil;
    self.mockLastNameValidator = nil;
}

- (void)testFirstNameInvalid {
    STAssertFalse([self.validator validateNameWithFirst:invalidFirstName andLast:lastName], @"Invalid first name makes whole name invalid.");
    
    [verify(self.mockFirstNameValidator) validate:invalidFirstName];
}

- (void)testLastNameInvalid {
    STAssertFalse([self.validator validateNameWithFirst:firstName andLast:invalidLastName], @"Invalid last name makes whole name invalid.");
    
    [verify(self.mockFirstNameValidator) validate:firstName];
    [verify(self.mockLastNameValidator) validate:invalidLastName];
}

- (void)testBothNamesInvalid {
    STAssertFalse([self.validator validateNameWithFirst:invalidFirstName andLast:invalidLastName], @"Invalid first and last name makes whole name invalid.");
    
    [verify(self.mockFirstNameValidator) validate:invalidFirstName];
}

- (void)testBothNamesValid {
    STAssertTrue([self.validator validateNameWithFirst:firstName andLast:lastName], @"Valid first and last name makes whole name valid.");
    
    [verify(self.mockFirstNameValidator) validate:firstName];
    [verify(self.mockLastNameValidator) validate:lastName];
}

@end
