//
//  MUViewController.m
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 6/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import <QBFlatButton.h>

#import "MUViewController.h"

@interface MUViewController ()

- (IBAction)buttonPressed;

@end

@implementation MUViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    QBFlatButton *btn = [QBFlatButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(90, 200, 140, 50);
    
    btn.faceColor = [UIColor colorWithRed:0.333 green:0.631 blue:0.851 alpha:1.0];
    btn.sideColor = [UIColor colorWithRed:0.310 green:0.498 blue:0.702 alpha:1.0];
    
    btn.radius = 6.0;
    btn.margin = 4.0;
    btn.depth = 3.0;
    
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"Button" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btn];
}

- (IBAction)buttonPressed {
    [self presentViewController:[[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"NameValidationController"] animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
