//
//  NameValidationController.m
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import "NameValidationController.h"

#import "NameValidator.h"

@interface NameValidationController ()

@property (strong, nonatomic, readonly) NameValidator *nameValidator;

- (void)showNameError;
- (void)showSuccessView;

@end

@implementation NameValidationController

@synthesize nameValidator = _nameValidator;

- (NameValidator *)nameValidator {
    if (!_nameValidator) {
        _nameValidator = [[NameValidator alloc] initWithFirstNameValidator:[[FirstNameValidator alloc] init] andLastNameValidator:[[LastNameValidator alloc] init]];
    }
    return _nameValidator;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)validateButtonPressed {
    if (![self.nameValidator validateNameWithFirst:self.firstNameField.text andLast:self.lastNameField.text]) {
        [self showNameError];
    } else {
        [self showSuccessView];
    }
}

- (void)showNameError {
    [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Must have a first and last name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)showSuccessView {
    [self presentViewController:[[UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil] instantiateViewControllerWithIdentifier:@"SuccessController"] animated:YES completion:nil];
}

@end
