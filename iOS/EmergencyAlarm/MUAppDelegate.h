//
//  MUAppDelegate.h
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 6/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
