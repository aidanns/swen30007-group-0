//
//  LastNameValidator.h
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LastNameValidator : NSObject

- (BOOL)validate:(NSString *)name;

@end
