//
//  NameValidationController.h
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameValidationController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *validateButton;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;

- (IBAction)validateButtonPressed;

@end
