//
//  NameValidator.m
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import "NameValidator.h"

@interface NameValidator ()

@property (strong, nonatomic) FirstNameValidator *firstNameValidator;
@property (strong, nonatomic) LastNameValidator *lastNameValidator;

@end

@implementation NameValidator

@synthesize firstNameValidator = _firstNameValidator;
@synthesize lastNameValidator = _lastNameValidator;

- (id)initWithFirstNameValidator:(FirstNameValidator *)firstNameValidator andLastNameValidator:(LastNameValidator *)lastNameValidator {
    if (self = [super init]) {
        self.firstNameValidator = firstNameValidator;
        self.lastNameValidator = lastNameValidator;
    }
    return self;
}

- (BOOL)validateNameWithFirst:(NSString *)first andLast:(NSString *)last {
    return [self.firstNameValidator validate:first] && [self.lastNameValidator validate:last];
}

@end
