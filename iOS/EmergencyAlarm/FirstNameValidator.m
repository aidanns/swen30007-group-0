//
//  FirstNameValidator.m
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import "FirstNameValidator.h"

@implementation FirstNameValidator

- (BOOL)validate:(NSString *)name {
    return name.length != 0;
}

@end
