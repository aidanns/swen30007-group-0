//
//  NameValidator.h
//  EmergencyAlarm
//
//  Created by Aidan Nagorcka-Smith on 18/08/13.
//  Copyright (c) 2013 The University of Melbourne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FirstNameValidator.h"
#import "LastNameValidator.h"

@interface NameValidator : NSObject

- (id)initWithFirstNameValidator:(FirstNameValidator *)firstNameValidator andLastNameValidator:(LastNameValidator *)lastNameValidator;

- (BOOL)validateNameWithFirst:(NSString *)first andLast:(NSString *)last;

@end
