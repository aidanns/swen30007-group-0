
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// OCHamcrest
#define COCOAPODS_POD_AVAILABLE_OCHamcrest
#define COCOAPODS_VERSION_MAJOR_OCHamcrest 2
#define COCOAPODS_VERSION_MINOR_OCHamcrest 1
#define COCOAPODS_VERSION_PATCH_OCHamcrest 0

// OCMockito
#define COCOAPODS_POD_AVAILABLE_OCMockito
#define COCOAPODS_VERSION_MAJOR_OCMockito 0
#define COCOAPODS_VERSION_MINOR_OCMockito 23
#define COCOAPODS_VERSION_PATCH_OCMockito 0

// QBFlatButton
#define COCOAPODS_POD_AVAILABLE_QBFlatButton
#define COCOAPODS_VERSION_MAJOR_QBFlatButton 1
#define COCOAPODS_VERSION_MINOR_QBFlatButton 0
#define COCOAPODS_VERSION_PATCH_QBFlatButton 0

